import Point from "./point.js";
import Oscillator from "./oscillator.js"

export default class Thing {
  constructor(point, strengthOffset) {
    this.position = point || new Point();
    this.strengthOffset = strengthOffset;
  }
}
