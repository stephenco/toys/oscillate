import Thing from "./thing.js";
import Point from "./point.js";
import Oscillator from "./oscillator.js"

export default class World {
  constructor(canvas) {
    this.started = false;
    this.size = new Point(800, 600);
    this.canvas = canvas;
    this.things = [];

    const thingSize = 20;
    const min = 40;
    const max = 100;
    let add = 0.03;
    let start = min

    for (let x = 0; x < this.size.x; x += thingSize) {
      for (let y = 0; y < this.size.y; y += thingSize) {
        this.things.push(new Thing(new Point(x, y), start));

        start += add;
        if (start > max) {
          start = max;
          add *= -1;
        }
        else if (start < min) {
          start = min;
          add *= -1;
        }

      }
    }

    this.oscillator = new Oscillator(4000, 200, -50, 150);
  }

  render() {
    const c = this.canvas.getContext('2d');

    c.fillStyle = '#000';
    c.fillRect(0, 0, this.canvas.width, this.canvas.height);

    for (let thing of this.things) {
      let str = thing.strengthOffset + this.oscillator.value;
      if (str > 100) {
        str = 100 - (str - 100);
      }
      else if (str < 0) {
        str = 0;
      }
      let a = str * 0.01;
      c.fillStyle = 'rgba(255, 255, 255, ' + a + ')';
      c.fillRect(thing.position.x + 5, thing.position.y + 5, 10, 10);
    }

    c.beginPath();
    let radians = x => (Math.PI/180) * x;
    c.arc(this.size.x * 0.5, (this.size.y * 0.4) + this.oscillator.value, 25, 0, radians(360));
    c.fillStyle = '#8ad';
    c.fill();
    c.lineWidth = 2;
    c.stroke();
  }

  start() {
    if (!this.started) {
      console.log('starting world');
      this.started = true;

      this.oscillator.start();

      const self = this;
      (function animator() {
        if (self.started) {
          self.render();
          window.requestAnimationFrame(animator);
        }
      })();
    }
    return this;
  }

  stop() {
    console.log('stopping world');
    this.oscillator.stop();
    this.started = false;
    return this;
  }
}
