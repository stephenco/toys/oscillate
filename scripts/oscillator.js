export default class Oscillator {
  constructor(ms, steps, min, max, start) {
    ms = ms || 1000;
    steps = steps || 10;

    this.min = min || 0;
    this.max = max || 100;

    this.value = start || this.min;
    this.step = (this.max - this.min) / steps;

    this.timeoutMs = ms / steps;
    this.timeout = null;

    this.subscribers = [];
    this.started = false;
  }

  start() {
    if (!this.started) {
      console.log('starting oscillator');
      this.started = true;
      let self = this;
      (function runner() {
        self.update();
        self.timeout = setTimeout(runner, self.timeoutMs);
      })();
    }
    return this;
  }

  stop() {
    console.log('stopping oscillator');
    this.started = false;
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    return this;
  }

  subscribe(fn, key) {
    this.subscribers.push({ key: key || Math.random(), fn: fn });
    return this;
  }

  unsubscribe(key) {
    this.subscribers = this.subscribers.filter(x => x.key != key);
    return this;
  }

  update() {
    this.value += this.step;

    if (this.value > this.max) {
      this.value = this.max;
      this.step *= -1;
    }
    else if (this.value < this.min) {
      this.value = this.min;
      this.step *= -1;
    }

    for (let sub of this.subscribers) {
      sub.fn(this.value, sub.key, this);
    }
  }
}
